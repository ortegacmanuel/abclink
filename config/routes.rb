Rails.application.routes.draw do
  get 'buskado/show'
  get '/buskado', to: 'buskado#show', as: 'buskado'

  namespace :traduktor_ortografiko, path: "traduktor-ortografiko" do
    get 'fonologiko', to: 'traduktor_ortografiko#fonologiko'
    post 'fonologiko', to: 'traduktor_ortografiko#post_fonologiko'

    get 'etimologiko', to: 'traduktor_ortografiko#etimologiko'
    post 'etimologiko', to: 'traduktor_ortografiko#post_etimologiko'

    root 'traduktor_ortografiko#fonologiko'
  end


  root "buskado#show"
end
