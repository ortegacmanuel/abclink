class BuskadoController < ApplicationController
  def show
    @query = params[:query].nil? ? '' : params[:query]
    @elimina_otro_idioma = 'AND (esaki OR esei OR aki) -la -los -las'
    @search_data = nil

    return if @query.empty?

    @search_data = SearchService.new.call(
      @query,
      params[:start].nil? ? 1 : params[:start].to_i,
      @elimina_otro_idioma
    )
  end
end
