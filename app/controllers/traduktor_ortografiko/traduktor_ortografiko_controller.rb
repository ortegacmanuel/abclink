module TraduktorOrtografiko
  class TraduktorOrtografikoController < ApplicationController
    def fonologiko
      @teksto = params[:teksto] || ""
      @resultado = OrthographicTranslatorService.new.call(
        @teksto,
        "aw"
      )
    end

    def post_fonologiko
      @resultado = OrthographicTranslatorService.new.call(
        params[:teksto],
        "cw"
      )

      render turbo_stream: turbo_stream.replace(
        "output_area",
        partial: "traduktor_ortografiko/output",
        locals: {
          resultado: @resultado
        }
      )
    end

    def etimologiko
      @teksto = params[:teksto] || ""
      @resultado = OrthographicTranslatorService.new.call(
        @teksto,
        "cw"
      )
    end

    def post_etimologiko
      @resultado = OrthographicTranslatorService.new.call(
        params[:teksto],
        "aw"
      )

      render turbo_stream: turbo_stream.replace(
        "output_area",
        partial: "traduktor_ortografiko/output",
        locals: {
          resultado: @resultado
        }
      )
    end


  end
end
