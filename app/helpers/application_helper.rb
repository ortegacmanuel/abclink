module ApplicationHelper
  def bold_search_words(snippet, words)
    snippet.split.collect { |w|
      words.include?(w.downcase.delete('.()",')) ? "<b>#{w}</b>" : w
    }.join(' ').html_safe
  end
end
