class SearchService
  def call(query, start = 1, options = '')
    search_data = {}

    search_data[:query] = query
    search_data[:start] = start

    # PapiamentuVariantsService Adds query_with_variants
    # and words_with_variants to search_data
    search_data.merge!(PapiamentuVariantsService.new.call(query))

    search_data[:query_with_variants] += " #{options}"

    searcher = Google::Apis::CustomsearchV1::CustomSearchAPIService.new
    searcher.key = Rails.application.credentials.google[:search_client_key]

    search_data[:results] = searcher.list_cses(
      q: search_data[:query_with_variants],
      cx: Rails.application.credentials.google[:search_client_cx],
      start: start
    )

    search_data
  end
end